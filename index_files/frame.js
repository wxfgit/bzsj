﻿

//根据ID获取元素
function __(strID)
{
	return document.getElementById(strID);
}

//取得左上角坐标
function GetPos(objVal)
{
	var p={x:0,y:0},o=objVal;
	p.x=o.offsetLeft;
	while(o=o.offsetParent)p.x+=o.offsetLeft;
	o=objVal;
	p.y=o.offsetTop;
	while(o=o.offsetParent)p.y+=o.offsetTop;
	return p;
}

//
function AddFav(strUrl,strTitle)
{
	window.external.addFavorite(strUrl,strTitle);
}

//
function PrintArea(intIdx)
{
	var t=String(intIdx);
	if(t=='null'||t=='undefined')
		t='';
	bdhtml=window.document.body.innerHTML;
	sprnstr='<!--startprint'+t+'-->';
	eprnstr='<!--endprint'+t+'-->';
	prnhtml=bdhtml.substr(bdhtml.indexOf(sprnstr)+17);
	prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));
	window.document.body.innerHTML=prnhtml;
	window.print();
}

//全选/全不选checkbox
function SelectAll(chkaVal,boolStatus)
{
	var pintNum=chkaVal.length;
	while(pintNum-->0)
	{
		chkaVal[pintNum].checked=boolStatus;
	}
}

//求字符串长度(字节)
function ByteLen(strVal)
{
	var pt=strVal.length,pintLen=0;
	while(pt-->0)
		pintLen+=1+(strVal.charCodeAt(pt)>255);
	return pintLen;
}

//去除字符串两头空格
function Trim(strVal)
{
	return String(strVal).replace(/(^\s*)|(\s*$)/g,'');
}

//显示层
function ShowLayer()
{
	var i,v,obj,args=ShowLayer.arguments;
	for(i=0;i<args.length-1;i+=2)
	{
		obj=__(args[i]);
		v=args[i+1];
		obj.style&&(obj=obj.style,v=v==1?'visible':'hidden');
		obj.visibility=v;
	}
}

//缩小图片(保持宽高比)
function FitImage(ImgD,intMaxWidth,intMaxHeight)
{
	var image=new Image();
	image.src=ImgD.src;
	if(image.width>0&&image.height>0)
	{
		if(image.width/image.height>=intMaxWidth/intMaxHeight)
		{
			if(image.width>intMaxWidth)
			{
				ImgD.width=intMaxWidth;
				ImgD.height=(image.height*intMaxWidth)/image.width;
			}
			else
			{
				ImgD.width=image.width;
				ImgD.height=image.height;
			}
		}
		else
		{
			if(image.height>intMaxHeight)
			{
				ImgD.height=intMaxHeight;
				ImgD.width=(image.width*intMaxHeight)/image.height;
			}
			else
			{
				ImgD.width=image.width;
				ImgD.height=image.height;
			}
		}
	}
}

//缩小图片宽度(保持宽高比)
function FitImgWidth(objImg,intMaxWidth)
{
	if(objImg.width>intMaxWidth)
	{
		var i=objImg.height/objImg.width*intMaxWidth;
		objImg.width=intMaxWidth;objImg.height=i;
	}
}

//鼠标缩放图片
function WheelImg(o)
{
	var zoom=parseInt(o.style.zoom,10)||100;
	zoom+=event.wheelDelta/12;
	zoom>0&&(o.style.zoom=zoom+'%');
}

//设置图片预览
function SetPicPreview(txt,img)
{
	img.src=txt.value;
}

//编辑处理
//m:1粗,2斜,3下划线,4删除线,5水平居中,6连接,7图片
function TxtEx(ctx,m)
{
	var pfront="",prear="",pfont=new Array("b","i","u","s","cnt");
	if(m>=1&&m<=5)
		pfront="{"+pfont[m-1]+"}",prear="{/"+pfont[m-1]+"}";
	else if(m==6)
	{
		var phref=prompt("地址","http://");
		if(phref==null)
			return;
		pfront="{a "+phref+"}",prear="{/a}";
	}
	else if(m==7)
	{
		var pimg=prompt("图片地址","http://");
		if(pimg==null)
			return;
		pfront="{img0"+pimg+"}";
	}

	if(document.selection&&document.selection.type=="Text")
	{
		var prange=document.selection.createRange(),ptxt=prange.text;
		prange.text=pfront+ptxt+prear;
	}
	else
		ctx.value=pfront+ctx.value+prear;
}

function ShowTxtExBar()
{
	document.write("<img onclick=TxtEx(cctx,1) src=/img/bt/b.gif><img onclick=TxtEx(cctx,2) src=/img/bt/i.gif>\
<img onclick=TxtEx(cctx,3) src=/img/bt/u.gif><img onclick=TxtEx(cctx,4) src=/img/bt/s.gif>\
<img onclick=TxtEx(cctx,5) src=/img/bt/cnt.gif><img onclick=TxtEx(cctx,6) src=/img/bt/a.gif>\
<img onclick=TxtEx(cctx,7) src=/img/bt/img.gif>");
}

function QueryDelete(tform,strTips)
{
	var c=0,t,tn=tform.cc.length,pstrTips=strTips;
	(String(pstrTips)=='null'||String(pstrTips)=='undefined'||String(pstrTips)=='')&&(pstrTips='确认要删除吗？删除后不可恢复！');
	String(tn)=="undefined"&&(tn=1);
	for(t=0;t<tn;t++)
		tform.cc[t].checked&&c++;
	if(!c||!confirm(pstrTips))
		return false;
}

function QueryDel1(url,strTips)
{
	var pstrTips=strTips;
	(String(pstrTips)=='null'||String(pstrTips)=='undefined'||String(pstrTips)=='')&&(pstrTips='确认要删除吗？删除后不可恢复！');
	confirm(pstrTips)&&(location=url);
}

function QueryDel2(strTips)
{
	var pstrTips=strTips;
	(String(pstrTips)=='null'||String(pstrTips)=='undefined'||String(pstrTips)=='')&&(pstrTips='确认要删除吗？删除后不可恢复！');
	return confirm(pstrTips);
}

//var gan="甲乙丙丁戊己庚辛壬癸",zhi="子丑寅卯辰巳午未申酉戌亥",shu="鼠牛虎兔龙蛇马羊猴鸡狗猪";
/*
function Dat()
{
	var n=new Date(),day="天一二三四五六",cl="<font color=blue>";
	var y=n.getFullYear(),w=n.getDay();
	w==0&&(cl="<font color=red>");
	w==6&&(cl="<font color=green>");
	return cl+y+"年"+String(n.getMonth()+1)+"月"+n.getDate()+"日 星期"+day.charAt(w)+" "+gan.charAt((y+36-1900)%10)+zhi.charAt((y+36-1900)%12)+shu.charAt((y-4)%12)+"年</font>";
}


function Tim()
{
	var n=new Date(),ih=n.getHours(),im=n.getMinutes(),is=n.getTime()%60000;
	is=(is-is%1000)/1000;
	var clk=zhi.charAt(parseInt((ih+1)/2)%12)+"时"+ih+":";
	im<10&&(clk+="0");
	clk+=im+":";
	is<10&&(clk+="0");
	clk+=is;
	return clk;
}

function iClk()
{
	__("ca1").innerHTML=Dat(),__("ca2").innerHTML=Tim();
}

function Calinder()
{
	document.write("<table width=100%><tr><td id=ca1><tr><td id=ca2></table>");
	iClk(),setInterval("iClk()",1000);
}
*/



function GetHttpRequest()
{
	var MSXML=new Array('msxml2.XMLHTTP.5.0','msxml2.XMLHTTP.4.0','msxml2.XMLHTTP.3.0','msxml2.XMLHTTP','Microsoft.XMLHTTP');
	var i=MSXML.length;
	while(i-->0)
	{
		try
		{
			return new ActiveXObject(MSXML[i]);
		}
		catch(e)
		{
		}
	}
	try
	{
		return new XMLHttpRequest();
	}
	catch(e)
	{
	}
	return 0;
}

function UrlEncode(strUrl)
{
	return strUrl.replace(/=/g,'%3D').replace(/&/g,'%26').replace(/\?/g,'%3F').replace(/%/g,'%25').replace(/ /g,'%20');
}






//显示后台操作
function ShowHt()
{
	CreateWin(550,380);
	RequestUrlData('/ht.asp','divWin');
}


//页面登录
function RequestLogin()
{
	CreateWin(160,160);
	__('divWin').innerHTML='<p class=\'a_c t_b\'>请先登录</p><p id=tLogin></p>';
	RequestUrlData('/user_.asp?fnc=quickrequest1','tLogin');
}

//直接登录(GET模式)
function RequestDLogin(strTarget)
{
	CreateWin(160,160);
	__('divWin').innerHTML='<p class=\'a_c t_b\'>请先登录</p><p id=tLogin></p>';
	RequestUrlData('/user_.asp?fnc=request1_&target='+strTarget,'tLogin');
}

//快速登录
function ShowQuickLogin(strEleId)
{
	RequestUrlData('/user_.asp?fnc=quickrequest1',strEleId);
}

//需要登录
function NeedLogin(strField)
{
	strField=String(strField);
	var cCur=GetHttpRequest(),t;
	if(cCur)
	{
		cCur.open('get','/user_.asp?fnc=curuser&rand='+String(Math.random()),1);
		cCur.send('');
		cCur.onreadystatechange=function()
		{
			if(cCur.readyState==4)
			{
				/*try
				{
					if((t=bytes2BSTR(cCur.responseBody))=='')
						RequestLogin();
				}
				catch(e)*/
				{
					if((t=cCur.responseText)=='')
						RequestLogin();
				}
				strField!=''&&strField!='undefined'&&strField!='null'&&(__(strField).innerHTML=t);
			}
		}
	}
}

//需要直接登录
function NeedDLogin(strTarget)
{
	var cCur=GetHttpRequest(),t;
	if(cCur)
	{
		cCur.open('get','/user_.asp?fnc=curuser&rand='+String(Math.random()),1);
		cCur.send('');
		cCur.onreadystatechange=function()
		{
			if(cCur.readyState==4)
			{
				/*try
				{
					if((t=bytes2BSTR(cCur.responseBody))=='')
						RequestDLogin(strTarget);
					else
						eval(strTarget);
				}
				catch(e)*/
				{
					if((t=cCur.responseText)=='')
						RequestDLogin(strTarget);
					else
						eval(strTarget);
				}
			}
		}
	}
}

//后台请求url
function RequestUrl(strUrl)
{
	strUrl=String(strUrl);
	strUrl+=strUrl.indexOf("?")>=0?"&":"?";
	var cUrl=GetHttpRequest();
	if(cUrl)
	{
		cUrl.open('get',strUrl+'rand='+String(Math.random()),1);
		cUrl.send('');
	}
}

//后台请求url的数据
function RequestUrlData(strUrl,strField)
{
	strUrl=String(strUrl);
	strUrl+=strUrl.indexOf("?")>=0?"&":"?";
	var cUrl=GetHttpRequest(),t,i,j;
	if(cUrl)
	{
		cUrl.open('get',strUrl+'rand='+String(Math.random()),1);
		cUrl.send('');
		cUrl.onreadystatechange=function()
		{
			if(cUrl.readyState==4)
			{
				/*try
				{
					t=bytes2BSTR(cUrl.responseBody);
				}
				catch(e)*/
				{
					t=cUrl.responseText;
				}
				while((i=t.indexOf('<script'))>=0&&(j=t.indexOf('</script>'))>0)
				{
					var p=t.substr(i,j-i),s=t.substr(i+7,100);
					s.indexOf('>')>0&&(s=s.substr(0,s.indexOf('>')));
					if(s.indexOf('src=')>0)
					{
						s=s.substr(s.indexOf('src=')+4,s.length);
						s.indexOf(' ')>0&&(s=s.substr(0,s.indexOf(' ')));
						s.substr(0,1)=='"'&&(s=s.substr(1,s.length),s=s.substr(0,s.indexOf('"')));
						s.substr(0,1)=='\''&&(s=s.substr(1,s.length),s=s.substr(0,s.indexOf('\'')));
					}
					else
						s='';
					p=p.substr(p.indexOf('>')+1,p.length);

					var oHead = document.getElementsByTagName('HEAD').item(0);
					var oScript = document.createElement('script');
					oScript.language = 'javascript';
					oScript.type ='text/javascript';
					//oScript.id = sId;
					//oScript.defer = true;
					s!=''&&(oScript.src=s);
					oScript.text=p;
					oHead.appendChild( oScript ); 

					t=(i?t.substr(0,i):'')+t.substr(j+9,t.length);
				}
				strField!=''&&strField!='undefined'&&strField!='null'&&(__(strField).innerHTML=t);
			}
		}
	}
}

//后台请求url的数据并函数返回
function RequestUrlCall(strUrl,fncCall)
{
	strUrl=String(strUrl);
	strUrl+=strUrl.indexOf("?")>=0?"&":"?";
	var cUrl=GetHttpRequest(),t,i,j;
	if(cUrl)
	{
		cUrl.open('get',strUrl+'rand='+String(Math.random()),1);
		cUrl.send('');
		cUrl.onreadystatechange=function()
		{
			if(cUrl.readyState==4)
			{
				/*try
				{
					t=bytes2BSTR(cUrl.responseBody);
				}
				catch(e)*/
				{
					t=cUrl.responseText;
				}
				fncCall(t);
			}
		}
	}
}


var FestivalDay={getDay:function(id){var _=[];_['0101']='元旦';_['0106']='小寒';_['0115']='腊八';_['0121']='大寒';_['0204']='立春';_['0206']='除夕';_['0207']='春节';_['0214']='情人节';_['0219']='雨水';_['0221']='元宵节';_['0303']='全国爱耳日';_['0305']='惊蛰';_['0308']='国际妇女节';_['0312']='植树节';_['0315']='消费者权益日';_['0320']='春分';_['0321']='世界森林日';_['0322']='世界水日';_['0323']='世界气象日';_['0401']='国际愚人节';_['0405']='清明节';_['0407']='世界卫生日';_['0420']='谷雨';_['0422']='世界地球日';_['0501']='国际劳动节';_['0504']='青年节';_['0505']='立夏';_['0508']='世界红十字日';_['0510']='母亲节';_['0512']='国际护士节';_['0517']='世界电信日';_['0521']='小满';_['0531']='世界无烟日';_['0601']='国际儿童节';_['0605']='芒种';_['0606']='全国爱眼日';_['0608']='端午节';_['0621']='父亲节 夏至';_['0623']='国际奥林匹克日';_['0626']='国际禁毒日';_['0701']='中国共产党诞辰';_['0707']='小暑 抗战纪念日';_['0722']='大暑';_['0801']='建军节';_['0807']='立秋 七夕';_['0812']='国际青年日';_['0823']='处暑';_['0907']='白露';_['0910']='中国教师节';_['0914']='中秋节';_['0918']='九一八纪念日';_['0920']='全国爱牙日';_['0922']='秋分';_['1001']='国庆节';_['1007']='重阳节';_['1008']='寒露';_['1015']='国际盲人节';_['1023']='霜降';_['1107']='立冬';_['1108']='中国记者节';_['1109']='中国消防宣传日';_['1110']='世界青年节';_['1117']='国际大学生节';_['1122']='小雪/感恩节';_['1201']='世界艾滋病日';_['1203']='世界残疾人日';_['1207']='大雪';_['1209']='一二九纪念日';_['1212']='西安事变纪念日';_['1221']='冬至';_['1225']='圣诞节';return _[id];},getWeek:function(id){var _=['星期日','星期一','星期二','星期三','星期四','星期五','星期六'];return _[id];},getString:function(){var now=new Date();var y=now.getFullYear();var m=now.getMonth()+1;var d=now.getDate();var w=now.getDay();m<10?m=0+''+m:0;d<10?d=0+''+d:0;var index=m+''+d;var festival=this.getDay(index);if(!festival||y!=2008){festival='';}var r=y+'年'+(m-0)+'月'+(d-0)+'日　'+this.getWeek(w)+'　'+festival;return r;},show:function(){document.write(this.getString());}}





function getCookieByName(name)
{
	var arr,reg=new RegExp("(^|)"+name+"=([^;]*)(;|$)");
	if(arr=document.cookie.match(reg))
		return arr[2];
	else
		return null;
}

function n2(intVal)
{
	if(intVal==2||intVal==3||intVal==4||intVal==5)
		__('n2'+String(intVal)).className='pb';
}

function n2a(intVal)
{
	if(intVal==2||intVal==3||intVal==4||intVal==5)
		__('n2'+String(intVal)).className='pn';
}






function Search()
{
	var pin=document.cFormSearch;
	var t=pin.sel.value;
	if(t=='2')
		pin.action='';
	else if(t=='3')
		pin.action='';
	else if(t=='4')
		pin.action='';
	else if(t=='5')
		pin.action='';
}








var pstrCss;
var K=function(obj)
{
	return function()
	{
		//pstrCss=obj.className;
		if(obj.className.indexOf('hui')>=0)
			obj.className='hui g_yw';
		else
			obj.className='g_yw';
	}
}
var G=function(obj)
{
	return function()
	{
		if(obj.className.indexOf('hui')>=0)
			obj.className='hui';//pstrCss;
		else
			obj.className='';//pstrCss;
	}
}
function BbInit()
{
	pstrCss='';
	var p=__('tBb').getElementsByTagName('tr');
	var n=p.length;
	while(n-->0)
	{
		try
		{
			p[n].addEventListener('mouseover',K(p[n]),false);
			p[n].addEventListener('mouseout',G(p[n]),false);
		}
		catch(ex)
		{
			p[n].attachEvent('onmouseover',K(p[n]));
			p[n].attachEvent('onmouseout',G(p[n]));
		}
	}
}




function FavZt(intUID)
{
	CreateWin(600,300);
	__('divWinTitle').innerHTML='添加收藏';
	RequestUrlData('/fav_.asp?fnc=edit01&item=41&obj='+String(intUID),'divWin');
}




function Page2Pi(strVal)
{
	var p=strVal;
	p=p.replace(/\<br\>/g,"\r\n").replace(/\<br\/\>/g,"\r\n").replace(/\<br \/\>/g,"\r\n");
	p=p.replace(/\<BR\>/g,"\r\n").replace(/\<BR\/\>/g,"\r\n").replace(/\<BR \/\>/g,"\r\n");
	return p;
}

function Pi2Txt()
{
	var p=Page2Pi(__('tPi2').innerHTML);
	var i=0,t,c=0;
	while((t=p.indexOf('<input '))>=0||(t=p.indexOf('<INPUT '))>=0)
	{
		var a=p.substr(t,p.length);
		a=a.substr(a.indexOf(">")+1,a.length);
		if(!__('cPi'+String(++i)).checked)
		{
			a=a.substr(a.indexOf('<!-- -->'),a.length);
			a=a.replace('\r\n','');
		}
		else
			a=String(++c)+'.'+a;
		p=p.substr(0,t)+a;
		p=p.replace('<!-- -->','');
	}
	if(c==1)
		p=p.replace('1.','');
	__('tPiEditArea').value=p;
}

function CutPi(intPi)
{
	if(intPi==0)
	{
		__('tPi1').className='pn';
		__('tPi2').className='pn';
		__('tPiEdit').className='pn';
		__('tPiEditCtx').className='';
	}
	else
	{
		__('tPi1').className=intPi==1?'':'pn';
		__('tPi2').className=intPi==1?'pn':'';
		__('tPiEdit').className='';
		__('tPiEditCtx').className='pn';
		if(intPi==1)
			__('tPiEditArea').value=Page2Pi(__('tPi1').innerHTML);
		else
			Pi2Txt();
	}
}

function ShowTk()
{
	CreateWin(640,480);
	RequestUrlData('/page/tiaokuan.htm','divWin');
}

