﻿
var uaWinBg=new Array(),uaWin=new Array(),
	uintaWinWidth=new Array(),uintaWinHeight=new Array(),
	uboolaWinClk=new Array(),uintaWinPreX=new Array(),uintaWinPreY=new Array(),uintWinN=0;

//创建窗口
//返回窗口序号
function CreateWin(intWidth,intHeight)
{
	uintaWinWidth[uintWinN]=intWidth,uintaWinHeight[uintWinN]=intHeight;
	uboolaWinClk[uintWinN]=0;
	uintaWinPreX[uintWinN]=uintaWinPreY[uintWinN]=0;
	var pw=document.body.scrollWidth,ph=document.body.scrollHeight,
		cw=document.documentElement.clientWidth,ch=document.documentElement.clientHeight,
		sw=document.documentElement.scrollLeft,sh=document.documentElement.scrollTop;
	if(pw<cw)
		pw=cw;
	if(ph<ch)
		ph=ch;
	uaWinBg[uintWinN]=document.createElement('div');
	uaWinBg[uintWinN].className='winbg winbgb';
	uaWinBg[uintWinN].style.zIndex=String(9000+10*uintWinN);
	uaWinBg[uintWinN].style.width=String(pw)+'px';
	uaWinBg[uintWinN].style.height=String(ph)+'px';
	uaWinBg[uintWinN].innerHTML='<iframe width=100% height=100% class=winbgb></iframe>';
	document.body.appendChild(uaWinBg[uintWinN]);

	uaWin[uintWinN]=document.createElement('div');
	uaWin[uintWinN].className='win';
	var wz='z-index:'+String(9001+10*uintWinN)+';';
	var pstrVer=window.navigator.userAgent.toLowerCase();
	if(pstrVer.indexOf('msie 6.0')>0&&pstrVer.indexOf('msie 8.0')<0&&pstrVer.indexOf('msie 7.0')<0)
	{
		//ie6.0
		uaWin[uintWinN].style.cssText=wz+'position:absolute;left:expression(offsetParent.scrollLeft+(offsetParent.clientWidth-'+String(intWidth)
			+')/2);top:expression(offsetParent.scrollTop+(offsetParent.clientHeight-'+String(intHeight)+')/2);';
	}
	else
	{
		//其他浏览器
		uaWin[uintWinN].style.cssText=wz+'position:fixed;';
		uaWin[uintWinN].style.left=String(/*sw+*/(cw-intWidth)/2-10)+'px';
		uaWin[uintWinN].style.top=String(/*sh+*/(ch-intHeight)/2-10<0?0:/*sh+*/(ch-intHeight)/2-10)+'px';
	}
	uaWin[uintWinN].style.width=String(intWidth)+'px';
	uaWin[uintWinN].style.height=String(intHeight)+'px';
	uaWin[uintWinN].innerHTML='<div class=wintitle onmousedown=uboolaWinClk[uintWinN-1]=1; onmouseout=uboolaWinClk[uintWinN-1]=0; onmouseup=uboolaWinClk[uintWinN-1]=0; onmousemove=MoveWin(event);>'
		+'<li id=divWinTitle'+(uintWinN?String(uintWinN):'')+' class="f_l pd_10t"></li>'
		+'<li class=\'f_r w17\'><img id=bClose class=cr_hand title=关闭窗口 onclick=CloseWin(); src=/img/btWinClose.png></li>'
		+'<div class=c_b></div></div>'
		+'<div id=divWin'+(uintWinN?String(uintWinN):'')+' class=\'pd_10 ox_h oy_h\'></div>';
	document.body.appendChild(uaWin[uintWinN]);
	/*try
	{
		__('bClose').addEventListener('click',CloseWin,false);
	}
	catch(ex)
	{
		__('bClose').attachEvent('onclick',CloseWin);
	}*/
	return uintWinN++;
}

//关闭窗口,按打开顺序反关
function CloseWin()
{
	if(uintWinN)
	{
		--uintWinN;
		document.body.removeChild(uaWin[uintWinN]);
		document.body.removeChild(uaWinBg[uintWinN]);
	}
}

function MoveWin(event)
{
	if(uboolaWinClk[uintWinN-1])
	{
		if(uintaWinPreX[uintWinN-1]==0&&uintaWinPreY[uintWinN-1]==0)
			uintaWinPreX[uintWinN-1]=event.clientX,uintaWinPreY[uintWinN-1]=event.clientY;
		else
		{
			var x=parseInt(uaWin[uintWinN-1].style.left)+event.clientX-uintaWinPreX[uintWinN-1],y=parseInt(uaWin[uintWinN-1].style.top)+event.clientY-uintaWinPreY[uintWinN-1];
			if(x>0&&y>0&&x<document.documentElement.clientWidth-uintaWinWidth[uintWinN-1]&&y<document.documentElement.clientHeight-uintaWinHeight[uintWinN-1])
				uaWin[uintWinN-1].style.left=String(x)+'px',uaWin[uintWinN-1].style.top=String(y)+'px';
			uintaWinPreX[uintWinN-1]=event.clientX,uintaWinPreY[uintWinN-1]=event.clientY;
		}
		try
		{
			window.getSelection().removeAllRanges();
 		}
		catch(ex)
		{
			document.selection.empty();
		}
	}
	else
		uintaWinPreX[uintWinN-1]=uintaWinPreY[uintWinN-1]=0;
}
